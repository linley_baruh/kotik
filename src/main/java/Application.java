import animals.Kotik;
public class Application {

    public static void main(String[] args) {

        Kotik fil = new Kotik("Филя","Мур", 1, 8 );

        Kotik barsik = new Kotik();
        barsik.setName("Барсик");
        barsik.setVoice("Мур");
        barsik.setSatiety(6);
        barsik.setWeight(10);

        System.out.println(fil.getName() + ":" );
        for(String arrayLiveAnotherDay : fil.liveAnotherDay()){
            System.out.println(arrayLiveAnotherDay);
        }

        System.out.println(barsik.getName() + " весит " + barsik.getWeight() +"кг");

        boolean voicesAreEqual = compareVoice(fil, barsik);
        if (voicesAreEqual) {
            System.out.println("Котики разговаривают одинаково.");
        } else {
            System.out.println("Котики разговаривают по-разному.");
        }

        System.out.println("Количество котиков: " + Kotik.getCount());

    }

    public static boolean compareVoice (Kotik fil, Kotik barsik){
        return fil.getVoice().equals(barsik.getVoice());
    }
}

