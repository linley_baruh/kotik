package animals;

public class Kotik {
    private String name;
    private String voice;
    private int satiety;
    private int weight;

    private String nameEat;
    private int satietyEat;

    private static int count = 0;
    private final int METHODS = 5;

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getVoice(){
        return voice;
    }
    public void setVoice(String voice){
        this.voice = voice;
    }
    public int getSatiety(){
        return satiety;
    }
    public void setSatiety(int satiety){
        if(satiety > 0) this.satiety = satiety;
        else System.out.print("Ошибка! Сытость не может быть отрицательным числом");
    }
    public int getWeight(){
        return weight;
    }
    public void setWeight(int weight){
        if(weight > 0) this.weight = weight;
        else System.out.println("Ошибка! Вес не может быть отрицательным числом");
    }

    public String getNameEat(){
        return nameEat;
    }
    public void setNameEat(String nameEat){
        this.nameEat = nameEat;
    }
    public int getSatietyEat(){
        return satietyEat;
    }
    public void setSatietyEat(int satietyEat){
        this.satietyEat = satietyEat;
    }

    public static int getCount(){
        return count;
    }

    public Kotik(String name, String voice, int satiety, int weight) {
        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        count++;
    }

    public Kotik() {
        count++;
    }


    public boolean play() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else return false;
    }

    public boolean sleep() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else return false;
    }

    public boolean wash() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else return false;
    }

    public boolean walk() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else return false;
    }

    public boolean hunt() {
        if (satiety > 0) {
            satiety--;
            return true;
        } else return false;
    }


    public void eat(int satietyEat) {
        this.satietyEat = satietyEat;
        satiety += satietyEat;
    }

    public void eat(int satietyEat, String name_eat) {
        this.satietyEat = satietyEat;
        this.nameEat = nameEat;
        satiety += satietyEat;
    }

    public void eat() {
        eat(2, "Корм");
    }

    public String[] liveAnotherDay() {
        String[] arrayStr = new String[24];
        int hour = 0;
        while (hour < 24) {
            switch ((int) (Math.random() * METHODS) + 1) {
                case 1:
                    if (play()) {
                        arrayStr[hour] = hour + " - гулял";
                    } else {
                        eat(5);
                        arrayStr[hour] = hour + " - ел";
                    }
                    hour++;
                    break;
                case 2:
                    if (sleep()) {
                        arrayStr[hour] = hour + " - спал";
                    } else {
                        eat(5);
                        arrayStr[hour] = hour + " - ел";
                    }
                    hour++;
                    break;
                case 3:
                    if (wash()) {
                        arrayStr[hour] = hour + " - умывался";
                    } else {
                        eat(5);
                        arrayStr[hour] = hour + " - ел";
                    }
                    hour++;
                    break;
                case 4:
                    if (walk()) {
                        arrayStr[hour] = hour + " - гулял";
                    } else {
                        eat(5);
                        arrayStr[hour] = hour + " - ел";
                    }
                    hour++;
                    break;
                case 5:
                    if (hunt()) {
                        arrayStr[hour] = hour + " - охотился";
                    } else {
                        eat(5);
                        arrayStr[hour] = hour + " - ел";
                    }
                    hour++;
                    break;
            }
        }
        return arrayStr;
    }
}
